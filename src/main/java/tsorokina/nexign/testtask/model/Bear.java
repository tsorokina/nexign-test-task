package tsorokina.nexign.testtask.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Bear {

    @JsonProperty("bear_id")
    private Integer bearId;

    @JsonProperty("bear_type")
    private Bear.Type bearType;

    @JsonProperty("bear_name")
    private String bearName;

    @JsonProperty("bear_age")
    private double bearAge;


    public enum Type {
        POLAR,
        BROWN,
        BLACK,
        GUMMY
    }

}
