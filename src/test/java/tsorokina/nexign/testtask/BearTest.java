package tsorokina.nexign.testtask;

import io.restassured.http.ContentType;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.Response;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import tsorokina.nexign.testtask.model.Bear;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;


public class BearTest extends BaseTest {

    @ParameterizedTest
    @EnumSource(Bear.Type.class)
    @DisplayName("Create a bear")
    public void createBearTest(Bear.Type bearType) {
        Response response = given()
                .contentType(ContentType.JSON)
                .body(new Bear().setBearType(bearType)
                        .setBearName(RandomStringUtils.randomAlphanumeric(10))
                        .setBearAge(RandomUtils.nextDouble(0, 100)))
                .when()
                .post();

        assertThat("Response status code is not 200", response.getStatusCode(), equalTo(200));
    }


    @Test
    @DisplayName("Check count of bears")
    public void checkCountOfBearsTest() {
        List<Bear> observedBears = getListOfBears();

        assertThat("Bears count is not as expected", observedBears.size(), equalTo(4));
    }

    @Test
    @DisplayName("Change bear name")
    public void changeBearNameTest() {
        String newBearName = "new bear name";
        Bear observedBear = getListOfBears().get(0);

        given()
                .body(observedBear.setBearName(newBearName))
                .put(observedBear.getBearId().toString())
                .then()
                .statusCode(200);

        Bear resultBear = given()
                .get(observedBear.getBearId().toString())
                .getBody()
                .as(Bear.class, ObjectMapperType.JACKSON_2);

        assertThat("Bear update failed", resultBear, equalTo(observedBear.setBearName(newBearName)));
    }

    private List<Bear> getListOfBears() {
        Bear[] body = given()
                .get()
                .getBody()
                .as(Bear[].class, ObjectMapperType.JACKSON_2);

        return Arrays.asList(body);
    }

}
