package tsorokina.nexign.testtask;


import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static io.restassured.RestAssured.basePath;
import static io.restassured.RestAssured.baseURI;

@Slf4j
@Testcontainers
public abstract class BaseTest {

    @Container
    public static GenericContainer bearApp
            = new GenericContainer("pjyfiepolqkd/bear:1.0")
            .withExposedPorts(8091)
            .waitingFor(Wait.forListeningPort());


    @BeforeAll
    public static void testSetup() {
        baseURI = "http://"
                + bearApp.getContainerIpAddress()
                + ":" + bearApp.getMappedPort(8091);
        basePath = "bear";
    }

}
